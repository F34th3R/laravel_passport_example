<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Union extends Model
{
//    protected $table = "unions";
//    protected $fillable = ['name', 'user_id'];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
