<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UnionController extends Controller
{
    public function index() {
        $unions = Auth::user()->unions()->get();
        return response()->json(['data' => $unions], 200, [], JSON_NUMERIC_CHECK);
    }
}
